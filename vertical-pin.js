"use strict";

docReady(() => {
  const timelines = document.querySelectorAll(".timeline-section");
  timelines.forEach((timeline) => {
    const targets = timeline.querySelectorAll(".list li.card");

    gsap.set(".pin-list", { autoAlpha: 1 });
    const tl = gsap.timeline({
      defaults: {
        overwrite: "auto",
      },
      scrollTrigger: {
        start: "top top",
        end: "bottom center",
        scrub: true,
        trigger: ".timeline-scroll",
      },
    });

    const stagger = 2;
    const duration = 1;

    gsap.set(".list", { autoAlpha: 1 });
    const notLast = document.querySelectorAll(
      ".list li.card:not(:last-of-type)"
    );
    tl.set(targets, { transformOrigin: "center bottom" });
    tl.from(targets, {
      duration: duration,
      opacity: 0,
      rotationX: 0,
      rotationY: 0,
      scale: 1,
      stagger: stagger,
      y: 60,
    }).to(
      notLast,
      {
        duration: duration,
        opacity: 0,
        rotationX: 20,
        scale: 0.75,
        stagger: stagger,
        y: -100,
      },
      stagger
    );
  });
});
