<!-- KEY DATES SECTION -->
<?php

$id = isset($block['anchor']) && $block['anchor'] ? $block['anchor'] : $block['id'];
$css = isset($block['className']) ? $block['className'] : '';

$image = get_field('background');
$style = my_wp_is_mobile() || !$image ? 'background : #fff' : 'background-image :linear-gradient(0deg, white 10%, transparent, white 90%),  url(' . $image['url'] . ')';

?>

<div class="timeline-scroll" style="<?php echo $style;?>">

<section id="<?= $id ?>" class="key-date-section sticky-top timeline-section <?= $css ?>">
    <div class="pin-list">
        <ul class="list list-unstyled">
            <li>
                <h2 class="pin-timeline-trigger">
                    <?php _e('HISTORY','theme-socreativ')?>
                </h2>
            </li>
            <?php if ( have_rows('key') ): ?>
                <?php while ( have_rows('key') ) : the_row(); ?>
                    <li class="card">
                        <p class="f">
                            <?php the_sub_field('key'); ?>
                        </p>
                        <h3 class="fs-16 f">
                        <?php the_sub_field('texte'); ?>
                        </h3>
                    </li>
                <?php endwhile; ?>
            <?php endif; ?>

        </ul>
        <div class="arrows appear arrow-block ">
            <svg width="20" height="11" xmlns="http://www.w3.org/2000/svg">
            <path d="M10.00000014 7.41421367l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781zm0 0l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781z" fill="#FFF" fill-rule="nonzero" />
            </svg>
            <svg width="20" height="11" xmlns="http://www.w3.org/2000/svg">
                <path d="M10.00000014 7.41421367l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781zm0 0l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781z" fill="#FFF" fill-rule="nonzero" />
            </svg>
            <svg width="20" height="11" xmlns="http://www.w3.org/2000/svg">
                <path d="M10.00000014 7.41421367l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781zm0 0l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781z" fill="#FFF" fill-rule="nonzero" />
            </svg>
            <svg width="20" height="11" xmlns="http://www.w3.org/2000/svg">
                <path d="M10.00000014 7.41421367l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781zm0 0l1.41421356 1.41421356H8.58578658l1.41421356-1.41421356zm0 0L17.07106795.34314586h2.82842713l-9.89949494 9.89949494L.1005052.34314586h2.82842713l7.07106781 7.07106781z" fill="#FFF" fill-rule="nonzero" />
            </svg>
        </div>
    </div>
</div>
</section>

</div>
